USE [miSistema]
GO
/****** Object:  Table [dbo].[Clientes]    Script Date: 28/08/2018 03:51:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clientes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NULL,
	[ApellidoP] [varchar](50) NULL,
	[ApellidoM] [varchar](50) NULL,
	[Edad] [int] NULL,
	[Ocupacion] [varchar](50) NULL,
 CONSTRAINT [PK_Clientes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Clientes] ON 

INSERT [dbo].[Clientes] ([id], [Nombre], [ApellidoP], [ApellidoM], [Edad], [Ocupacion]) VALUES (2, N'javier', N'hernandez', N'suarez', 24, N'isc')
INSERT [dbo].[Clientes] ([id], [Nombre], [ApellidoP], [ApellidoM], [Edad], [Ocupacion]) VALUES (3, N'juan ', N'perez', N'perez', 12, N'estudiante')
INSERT [dbo].[Clientes] ([id], [Nombre], [ApellidoP], [ApellidoM], [Edad], [Ocupacion]) VALUES (5, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Clientes] OFF
